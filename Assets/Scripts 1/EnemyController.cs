﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour {

    public Transform player;

    public float moveSpeed = 5.0f;
    public float maxDist = 10f;
    public float minDist = 1.0f;
	
	void Start () {
        try
        {
            player = GameObject.FindGameObjectWithTag("Player").transform;
        }
        catch
        {
            Debug.Log("Player is dead, cannot spawn enemy");
        }
	}
	
	void Update ()
    {
        if (player)
        {
            transform.LookAt(player);
            transform.Rotate(new Vector3(0, -90, 0), Space.Self);

            if (Vector3.Distance(transform.position, player.position) >= minDist)
            {
                transform.Translate(new Vector3(moveSpeed * Time.deltaTime, 0, 0));
            }
        }
        else
        {
            Destroy(gameObject);
        }

    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mover : MonoBehaviour {

    public float speed;
    public Rigidbody2D rb;
    public Vector2 vel = new Vector2(0, 10);

    Vector3 mousePosition;

	void Start () {
        rb = GetComponent<Rigidbody2D>();
        //    rb.velocity = transform.forward * speed;
        mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        mousePosition.Normalize();        
    }

    void Update()
    {
        transform.position += new Vector3(mousePosition.x, mousePosition.y) * speed;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameState : MonoBehaviour
{
    private Text infoScore;
    static string info;
    public static int score;
    public static bool stop;

    static GameState()
    {
        score = 0;
        stop = false;
    }

    void Start()
    {
        infoScore = GameObject.Find("Canvas").GetComponentInChildren<Text>();
    }

    void Update()
    {
        if (Input.GetMouseButton(0) && stop) StartGame();
        infoScore.text = "SCORE: " + score.ToString() + "\n" + info;
    }

    public static void IncreaseScore()
    {
        if (!stop) score += 1;
    }

    public static void DecreaseScore()
    {
        if (!stop && score != 0) score -= 1;
    }

    public static void StopGame()
    {
        stop = true;
        info = "YOU ARE LOSE";
    }

    public static void StartGame()
    {
        stop = false;
        score = 0;
        info = "";
    }
}

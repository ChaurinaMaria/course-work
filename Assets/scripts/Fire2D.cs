﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Fire2D : MonoBehaviour
{   
    public float speed = 10; // скорость огня
    public Rigidbody2D bullet; // префаб нашего огня
    public Transform gunPoint; // точка рождения
    public float fireRate = 1; // скорострельность
    public Transform Rotate; // объект для вращения по оси Z

    // ограничение вращения
    public float minAngle = -180;
    public float maxAngle = 180;
    private float curTimeout;

    void Start()
    {
    }

    void SetRotation()
    {
        Vector3 mousePosMain = Input.mousePosition;
        Vector3 lookPos = Camera.main.ScreenToWorldPoint(mousePosMain);

        if (lookPos.x < transform.position.x)
        {
            lookPos = transform.position - lookPos;
        } else return;

        float angle = Mathf.Atan2(lookPos.y, lookPos.x) * Mathf.Rad2Deg;
        angle = Mathf.Clamp(angle, minAngle, maxAngle) + 90;

        Rotate.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
    }

    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            Fire();
        }
        else
        {
            curTimeout = 100;
        }   

//        if (Rotate) SetRotation();
    }

    void Fire()
    {
        curTimeout += Time.deltaTime;
        if (curTimeout > fireRate)
        {
            curTimeout = 0;
            Rigidbody2D clone = Instantiate(bullet, gunPoint.position, Quaternion.identity) as Rigidbody2D;
            clone.velocity = transform.TransformDirection(gunPoint.right * speed);
            clone.transform.right = gunPoint.right;
        }
    }
}

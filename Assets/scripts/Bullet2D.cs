﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet2D : MonoBehaviour
{
    void Start()
    {
        Destroy(gameObject, 20);
    }

    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.isTrigger)
        {
           switch (coll.tag)
            {
                case "Enemy":
                    {
                        GameState.IncreaseScore();
                        break;
                    }
                case "Friend":
                    {
                        GameState.StopGame();
                        break;
                    }
            }

            Destroy(gameObject);
            Destroy(coll.gameObject);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Aircrafts2DGenerator : MonoBehaviour {
    float timeLeft = 3.0f;
    public Rigidbody2D AircraftFriend;
    public Rigidbody2D AircraftEnemy;
    private float curTimeout;
    private float aircraftSpeed = 10;
    private static bool stop;

    static Aircrafts2DGenerator()
    {
    }

    void Start () {
        curTimeout = 0;
    }
	
	void Update () {
        if (GameState.stop) return;

        curTimeout -= Time.deltaTime;
        if (curTimeout <= 0.0f) {
            curTimeout = timeLeft;
            float ty = Random.Range(-4.0f, 4.0f);
            float i = Random.Range(0.0f, 1.0f);
            Rigidbody2D rbAircraft = Instantiate((i > 0.5f) ? AircraftFriend : AircraftEnemy, new Vector3(-15, ty, -7), Quaternion.identity, gameObject.transform) as Rigidbody2D;
            rbAircraft.velocity = transform.TransformDirection(Vector3.right * aircraftSpeed);
            rbAircraft.transform.right = Vector3.right;
        }
    }
}

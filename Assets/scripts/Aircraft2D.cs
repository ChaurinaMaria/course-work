﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class Aircraft2D : MonoBehaviour {
    private GameObject gun;
	// Use this for initialization
	void Start () {
        gun = GameObject.Find("gun");
        Destroy(gameObject, 20);
	}
	
	// Update is called once per frame
	void Update () {
	    if (transform.position.x > gun.transform.position.x + 3) // offset after x of gut position
        {
            if (gameObject.tag == "Enemy")
            {
                GameState.DecreaseScore();
            }
            Destroy(gameObject);
        }
	}
}
